# Colors

## Table of contents
* [Information](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [URL](#url)
* [Repository](#repository)

## Information
This project was created with the purpose to be a space where workers of Multiplica can check the new colors game of the diferentes products as websites, flyers, new designs and social networks.

## Technologies
Project is created with:
* Angular CLI: 10.1.0
* Node: v12.18.3

## Setup

### Development Server
Run `ng server --open` for a dev server. Navigate to http://localhost:4200/. The app will automatically reload if you change any of the source files.

### Build
* Run `ng build --prod --base-href ./` to build the project. 
* The build artifacts will be stored on `dist/colors` directory.
* Ready, the app is working on `dist/colors/index.html`.

### Url
This app is hosted on [https://angular-colors.vercel.app/](https://angular-colors.vercel.app/)

### Repository
This app is available on [https://bitbucket.org/ricardolxique/angular-colors/src/master/](https://bitbucket.org/ricardolxique/angular-colors/src/master/)

