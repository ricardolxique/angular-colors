import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { ClipboardService } from 'ngx-clipboard';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
	title = 'COLORES';
  	colorsArray;
  	colorsPage = 0;
  	titleAlert = "Hola";
  	valueInit = 1
  	valuePrev = 0;
  	valueNext = 0;

  	constructor(private http: HttpClient, private _clipboardService: ClipboardService) { }
 
  	ngOnInit() { 
  		this._clipboardService.copyResponse$.subscribe(re => {
            if (re.isSuccess) {
                alert('Color copiado en tu clipboard');
            }
        });
  		this.getColors();
  	}

  	getColors(): void {
	  	this.http.get<any>('https://reqres.in/api/colors')
		.subscribe(data => { 
			this.colorsArray = data.data;
			this.colorsPage  = data.total_pages;
			error => console.log("Error :: " + error)  
		});
		this.valueNext = this.valueInit + 1;
  	}

  	fnCopy(colorCopy) {
  		 this._clipboardService.copy(colorCopy);
 	}

 	next(value) {
 		this.http.get<any>('https://reqres.in/api/colors?page='+value)
		.subscribe(data => { 
			this.colorsArray = data.data;
			console.log(this.colorsArray);
			error => console.log("Error :: " + error)  
		});

		this.valueInit = value;
		this.valuePrev = value - 1;
		this.valueNext = value + 1;
 	}

 	previous(value) {
 		  	this.http.get<any>('https://reqres.in/api/colors?page='+value)
			.subscribe(data => { 
				this.colorsArray = data.data;
				console.log(this.colorsArray);
				error => console.log("Error :: " + error)  
			});
		this.valueInit = value;
		this.valuePrev = value - 1;
		this.valueNext = value + 1;

 	}

}